from matplotlib import pyplot as plt
from matplotlib import animation as a
import numpy as np
class Car():
	
	def __init__(self, x=0, dx=1, bumper = None):
		self.x = x
		self.bumper = bumper
		self.dx = dx
	def __repr__(self, x, bumper):
		self.x = x
		self.bumper = bumper
		self.dx = bumper.dx

	def move(self):
		self.x = self.x + self.dx
	
	def change_dx(self, dx):
		self.dx = dx
		
	def seeDeer(self, slow):
		#probability
		#acceleration change by fraction (slow)
		if np.random.randint(0,10) > 8 :
			self.dx = slow * self.dx
			print ("SEE DEER")
		
		
xArr = []
yArr = []		
vw = Car(20, 5, None)
volvo = Car()
volvo.x = 15
volvo.dx = vw.dx
volvo.bumper = vw
xArr.append(vw.x)
xArr.append(volvo.x)
yArr.append(20)
yArr.append(20)
plt.plot(xArr,yArr, 'ro')
plt.show()

del xArr [:]
del yArr [:]
for i in range(0,4):
	vw.seeDeer(.5)
	vw.move()
	volvo.move()
	xArr.append(vw.x)
	xArr.append(volvo.x)
	yArr.append(20)
	yArr.append(20)
plt.plot(xArr,yArr, 'ro')
plt.show()

#New additions here
car_list = []
car_pos = []
graph_y = []
for i in range(5):
	new_car = Car()
	car_list.append(new_car)
for n in range(5):
	if n-1>=0:
		car_list[n].x = car_list[n-1].x + 1
		car_list[n].bumper = car_list[n-1]
for i in range(5):
	car_pos.append(car_list[i].x)
	graph_y.append(20)
plt.plot(car_pos,graph_y,'ro')
plt.show()